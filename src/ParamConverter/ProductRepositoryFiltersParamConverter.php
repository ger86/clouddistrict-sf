<?php

namespace App\ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Model\Repository\ProductRepositoryFilters;

class ProductRepositoryFiltersParamConverter implements ParamConverterInterface {

    /**
     * {@inheritdoc}
     *
     * Check, if object supported by our converter
     */
    public function supports(ParamConverter $configuration) {
        $class = $configuration->getClass(); 
        return $class === ProductRepositoryFilters::class;
    }
 
    /**
     * {@inheritdoc}
     *
     * Applies converting
     */
    public function apply(Request $request, ParamConverter $configuration) {
        $name = $request->get('name', null);
        $priceBottom = $request->get('priceBottom', null);
        $priceTop = $request->get('priceTop', null);
        $categoryId = $request->get('categoryId', null);
        $filters = new ProductRepositoryFilters(
            $name,
            $priceBottom,
            $priceTop,
            $categoryId
        );
        $request->attributes->set($configuration->getName(), $filters);
    }
}