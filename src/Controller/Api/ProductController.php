<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Model\Repository\ProductRepositoryFilters;
use App\Repository\ProductRepository;

/**
 * @Rest\Route("api")
 */
class ProductController extends AbstractFOSRestController {

    /**
     * Gets the products filtered by some params
     *
     * @Rest\Get(path="/products")
     * @Rest\View(serializerGroups={"product"})
     * @ParamConverter("filters", class="App\Model\Repository\ProductRepositoryFilters")
     */
    public function getProductsAction(ProductRepository $productRepo, ProductRepositoryFilters $filters) {
        $products = $productRepo->findUsingFilters($filters);
        return $products;
    }

}
