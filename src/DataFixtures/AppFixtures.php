<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Product;
use App\Entity\ProductCategory;

class AppFixtures extends Fixture {

    public function load(ObjectManager $manager) {

        $categoryA = new ProductCategory();
        $categoryA->setName('category A');
        $manager->persist($categoryA);

        $categoryB = new ProductCategory();
        $categoryB->setName('category B');
        $manager->persist($categoryB);

        $categories = [$categoryA, $categoryB];

        for ($i = 0; $i < 20; $i++) {
            $product = new Product();
            $product->setName('product'.$i);
            $product->setPrice(($i + 1) * 10);
            $product->setCategory($categories[$i % 2]);
            $product->setDescription('a product description'.$i);
            $manager->persist($product);
        }
        $manager->flush();
    }
}