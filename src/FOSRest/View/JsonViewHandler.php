<?php

namespace App\FOSRest\View;

use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JsonViewHandler {

    /**
     * @param ViewHandler $viewHandler
     * @param View        $view
     * @param Request     $request
     * @param string      $format
     *
     * @return Response
     */
    public function createResponse(ViewHandler $handler, View $view, Request $request, $format) {
        $response = $handler->createResponse($view, $request, $format);
        $content = json_decode($response->getContent(), true);
        $response->setContent(json_encode(
            [
                'data' => $content
            ]
        ));
        return $response;
    }
}