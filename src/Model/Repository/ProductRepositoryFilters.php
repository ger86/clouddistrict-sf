<?php

namespace App\Model\Repository;

class ProductRepositoryFilters {

    private $name;
    private $bottomPrice;
    private $topPrice;
    private $categoryId;

    public function __construct(?string $name, ?float $bottomPrice, ?float $topPrice, ?int $categoryId) {
        $this->name = $name;
        $this->bottomPrice = $bottomPrice;
        $this->topPrice = $topPrice;
        $this->categoryId = $categoryId;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of bottomPrice
     */ 
    public function getBottomPrice()
    {
        return $this->bottomPrice;
    }

    /**
     * Set the value of bottomPrice
     *
     * @return  self
     */ 
    public function setBottomPrice($bottomPrice)
    {
        $this->bottomPrice = $bottomPrice;

        return $this;
    }

    /**
     * Get the value of topPrice
     */ 
    public function getTopPrice()
    {
        return $this->topPrice;
    }

    /**
     * Set the value of topPrice
     *
     * @return  self
     */ 
    public function setTopPrice($topPrice)
    {
        $this->topPrice = $topPrice;

        return $this;
    }

    /**
     * Get the value of categoryId
     */ 
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set the value of categoryId
     *
     * @return  self
     */ 
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }
}