<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Product;
use App\Model\Repository\ProductRepositoryFilters;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository {

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findUsingFilters(ProductRepositoryFilters $filters) {
        $queryBuilder = $this->createQueryBuilder('p');
        if ($filters->getName()) {
            $queryBuilder->andWhere('p.name LIKE :name')
            ->setParameter('name', "%{$filters->getName()}%");
        }
        if ($filters->getTopPrice()) {
            $queryBuilder->andWhere('p.price <= :topPrice')
            ->setParameter('topPrice', $filters->getTopPrice());
        }
        if ($filters->getBottomPrice()) {
            $queryBuilder->andWhere('p.price >= :bottomPrice')
            ->setParameter('bottomPrice', $filters->getBottomPrice());
        }
        if ($filters->getCategoryId()) {
            $queryBuilder->andWhere('p.category = :categoryId')
                ->setParameter('categoryId', $filters->getCategoryId());
        }
        return $queryBuilder->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
