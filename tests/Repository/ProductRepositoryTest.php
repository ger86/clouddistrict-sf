<?php

namespace App\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\DataFixtures\AppFixtures;
use App\Entity\Product;
use App\Entity\ProductCategory;
use App\Model\Repository\ProductRepositoryFilters;
use App\Tests\FixtureAwareTestCase;

class ProductRepositoryTest extends FixtureAwareTestCase  {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp() {
        $kernel = self::bootKernel();
        $this->addFixture(new AppFixtures());
        $this->executeFixtures();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindUsingFilters() {
        $productRepo = $this->entityManager->getRepository(Product::class);
        $productCategoryRepo = $this->entityManager->getRepository(ProductCategory::class);

        $categories = $productCategoryRepo->findAll();
        $oddCategory = $categories[0];
        $evenCategory = $categories[1];

        $filters = new ProductRepositoryFilters(null, null, null, null);
        $products = $productRepo->findUsingFilters($filters);
        $this->assertGreaterThan(0, count($products));

        $filters = new ProductRepositoryFilters('product3', null, null, null);
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(1, count($products));

        $filters = new ProductRepositoryFilters('product3', null, null, $evenCategory->getId());
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(1, count($products));

        $filters = new ProductRepositoryFilters('product3', null, null, $oddCategory->getId());
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(0, count($products));

        $filters = new ProductRepositoryFilters(null, null, 11, null);
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(1, count($products));

        $filters = new ProductRepositoryFilters(null, 0, 11, null);
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(1, count($products));

        $filters = new ProductRepositoryFilters(null, 0, 210, null);
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(20, count($products));

        $filters = new ProductRepositoryFilters('product', 0, 210, null);
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(20, count($products));

        $filters = new ProductRepositoryFilters('product', 0, 210, $oddCategory->getId());
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(10, count($products));

        $filters = new ProductRepositoryFilters('product', 300, 400, $oddCategory->getId());
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(0, count($products));

        $filters = new ProductRepositoryFilters('foo', 0, 210, null);
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(0, count($products));

        $filters = new ProductRepositoryFilters('product', null, null, -1);
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(0, count($products));

        $filters = new ProductRepositoryFilters('pr', 1, 30, $evenCategory->getId());
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(1, count($products));

        $filters = new ProductRepositoryFilters('pr', 1, 30, $oddCategory->getId());
        $products = $productRepo->findUsingFilters($filters);
        $this->assertEquals(2, count($products));
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}