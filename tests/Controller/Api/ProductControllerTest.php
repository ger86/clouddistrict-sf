<?php

namespace App\Tests\Controller\Api;

use Symfony\Component\HttpFoundation\Response;
use App\Tests\FixtureAwareWebTestCase;
use App\DataFixtures\AppFixtures;
use App\Entity\ProductCategory;

class ProductControllerTest extends FixtureAwareWebTestCase {

    protected function setUp() {
        $kernel = self::bootKernel();
        $this->addFixture(new AppFixtures());
        $this->executeFixtures();
    }

    public function testGetProducts() {
        $client = static::createClient();
        $container = $client->getContainer();
        $em = $container->get('doctrine')->getManager();
        $productCategoryRepo = $em->getRepository(ProductCategory::class);

        $categories = $productCategoryRepo->findAll();
        $oddCategory = $categories[0];
        $evenCategory = $categories[1];

        $crawler = $client->request('GET', '/api/products');
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertNotEmpty($products);
        $firstProduct = $products[0];
        $this->assertTrue(isset($firstProduct['id']));
        $this->assertTrue(isset($firstProduct['name']));
        $this->assertTrue(isset($firstProduct['price']));
        $this->assertTrue(isset($firstProduct['description']));
        $this->assertTrue(isset($firstProduct['category']));
        $this->assertTrue(isset($firstProduct['category']['id']));
        $this->assertTrue(isset($firstProduct['category']['name']));

        $crawler = $client->request('GET', '/api/products', ['name' => 'product3']);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(1, count($products));

        $crawler = $client->request('GET', '/api/products', ['name' => 'product3', 'categoryId' => $evenCategory->getId()]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(1, count($products));

        $crawler = $client->request('GET', '/api/products', ['name' => 'product3', 'categoryId' => $oddCategory->getId()]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(0, count($products));

        $crawler = $client->request('GET', '/api/products', ['priceTop' => 11]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(1, count($products));

        $crawler = $client->request('GET', '/api/products', ['priceBottom' => 0, 'priceTop' => 11]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(1, count($products));

        $crawler = $client->request('GET', '/api/products', ['priceBottom' => 0, 'priceTop' => 210]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(20, count($products));

        $crawler = $client->request('GET', '/api/products', [
            'name' => 'product',
            'priceBottom' => 0, 
            'priceTop' => 210
        ]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(20, count($products));

        $crawler = $client->request('GET', '/api/products', [
            'name' => 'product',
            'priceBottom' => 0, 
            'priceTop' => 210,
            'categoryId' => $oddCategory->getId()
        ]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(10, count($products));

        $crawler = $client->request('GET', '/api/products', [
            'name' => 'product',
            'priceBottom' => 300, 
            'priceTop' => 400,
            'categoryId' => $oddCategory->getId()
        ]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(0, count($products));

        $crawler = $client->request('GET', '/api/products', [
            'name' => 'foo'
        ]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(0, count($products));

        $crawler = $client->request('GET', '/api/products', [
            'name' => 'product',
            'categoryId' => -1
        ]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(0, count($products));

        $crawler = $client->request('GET', '/api/products', [
            'name' => 'pr',
            'priceBottom' => 1, 
            'priceTop' => 30,
            'categoryId' => $evenCategory->getId()
        ]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(1, count($products));

        $crawler = $client->request('GET', '/api/products', [
            'name' => 'pr',
            'priceBottom' => 1, 
            'priceTop' => 30,
            'categoryId' => $oddCategory->getId()
        ]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = $client->getResponse()->getContent();
        $products = $this->extractData($content);
        $this->assertEquals(2, count($products));

    }

    private function extractData($responseContent): array {
        $contentAsArray = json_decode($responseContent, true);
        $this->assertNotNull($contentAsArray);
        $this->assertTrue(isset($contentAsArray['data']));
        return $contentAsArray['data'];
    }

}