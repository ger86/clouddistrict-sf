# Descripción

Proyecto creado para la prueba técnica de Symfony de *Cloud District*

# ¿Cómo probar?

1. Instalar dependencias 

```composer install```

2. Configurar base de datos en el archivo ```.env.local``` y ejecutar:

```bin/console doctrine:database:create```
```bin/console doctrine:migrations:migrate```

3. Correr los tests

4. La ruta de la api se encuentra en el path ```api/products```.

Admite como query params: ```name```, ```categoryId```, ```topPrice``` y ```bottomPrice```

# Bundles Empleados

1. ```FOSRestBundle``` para la API junto con ```JMSSerializerBundle```

2. ```SensioExtraFramework``` para habilitar anotaciones y el componente ```ParamConverter```